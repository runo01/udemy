<?php

/*
 *
 *Plugin Name:  Recipe Email Rating
 *Description:  This plugin extends the recipe plugin
 *
 *
 */

//Hooks
    add_action( 'recipe_rate', function( $arr ){
        
        $post                               =   get_post( $arr['post_id'] );
        $user_email                         =   get_the_author_meta( 'user_email', $post->author );
        $subject                            =   'Your recipe has received a new rating';
        $message                            =   'Your recipe '. $post->title .' has recieved a new rating ' .arr['rating'];
        
        wp_mail( $user_email, $subject, $message );
        
    });