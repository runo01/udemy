<?php 


//wp creates a unique id for our post with the $post_id 
function r_save_post_admin( $post_id, $post, $update ) {
   //this checks if post has been previously created or its a fresh post
        if (!$update) {
            return;
        }
    
        $recipe_data                            =   array();
        $recipe_data['video_url']               =   esc_url_raw( $_POST['r_inputVideoUrl'] );
        $recipe_data['ingredients']             =   sanitize_text_field( $_POST['r_inputIngredients'] );
        $recipe_data['time']                    =   sanitize_text_field( $_POST['r_inputTime'] );
        $recipe_data['utensils']                =   sanitize_text_field( $_POST['r_inputUtensils'] );
        $recipe_data['utensils']                =   sanitize_text_field( $_POST['r_inputLevel'] );
        $recipe_data['meal_type']               =   sanitize_text_field( $_POST['r_inputMealType'] );
        $recipe_data['rating']                  =   0;
        $recipe_data['rating_count']            =   0;
        
        // update is beta because if you check if it hasnt been updated before it can handle creation and update
  //recipe_data is a unique key for our posts wp uses to recognise our posts
        update_post_meta( $post_id, 'recipe_data', $recipe_data );
}