<?php


        function r_settings_api(){
            
            register_setting( 'r_opts_groups', 'r_opts', 'r_opts_sanitize' );
            
            add_settings_section( 
            
                'recipe_settings',//id
                __( 'Recipe Settings', 'recipe' ), //title
                'r_settings_section', //callback function
                'r_opts_sections' //page-section in a page already created in options page 
            
            );
            
            add_settings_field(
            
                'rating_login_required', //id
                'User login required for rating recipes', //title
                'rating_login_required_cb', //cb
                'r_opts_sections', //page
                'recipe_settings' //section
            
            );
            
            
            add_settings_field(
            
                'recipe_submission_login_required', //id
                'User login required for submitting recipe', //title
                'recipe_submission_login_required_cb',// cb
                'r_opts_sections', //page
                'recipe_settings' //section
            
            );
            
         
        }

        function r_settings_section(){
            
            echo '<p> You can change your recipe settings here </p>';
        }

        function rating_login_required_cb(){
           
           $recipe_opts                        =   get_option( 'r_opts' );
           ?>
                       <select name="r_opts[rating_login_required]" id="rating_login_required">
                             <option value="1">No</option>
                             <option value="2"<?php echo $recipe_opts['rating_login_required'] == 2 ? "SELECTED": ""; ?>>Yes</option>  
                     </select>  
           
           <?php
       }    
            
        function recipe_submission_login_required_cb(){
           
           $recipe_opts                        =   get_option( 'r_opts' );
           ?>
                       <select name="r_opts[recipe_submission_login_required]" id="recipe_submission_login_required">
                             <option value="1">No</option>
                             <option value="2"<?php echo $recipe_opts['recipe_submission_login_required'] == 2 ? "SELECTED": ""; ?>>Yes</option>  
                     </select> 
           
           <?php
       } 

        function r_opts_sanitize( $input ){
            
            $input['rating_login_required']                          =   absint($input['rating_login_required']);
            $input['recipe_submission_login_required']               =   absint($input['recipe_submission_login_required']);
            
            return $input;
        }
            

