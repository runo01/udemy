<?php

        function r_plugin_opts_page(){
           
            
            $recipe_opts                        =   get_option( 'r_opts' );
            
           ?>
           <div class="wrap">
               
              <div class="panel panel-success">
                  <div class="panel-heading">
                      <h3 class="panel-title text-center"><?php _e( 'Recipe Options', 'recipe' ); ?></h3>
                  </div>
                  <div class="panel-body">
                   <?php if( isset($_GET['status']) && $_GET['status'] == 1 ): ?>
                    <div class="alert alert-success">
                       Options Updated Successfully 
                    </div>
                  <?php endif; ?>
                    <form action="admin-post.php" method="post">
                         <input type="hidden" name="action" value="r_save_options">
                         <?php wp_nonce_field( 'r_options_verify' ) ?>
                          <div class="form-group">
                            <label for=""><?php _e( 'User login required for rating recipes', 'recipe' ); ?></label>
                           <select name="r_rating_login_required" class="form-control">
                             <option value="1">No</option>
                            <option value="2"<?php echo $recipe_opts['rating_login_required'] == 2 ? "SELECTED": ""; ?>>Yes</option>  
                           </select>  
                    </div>
                     <div class="form-group">
                           <label for=""><?php _e( 'User login required for submitting recipe', 'recipe' ); ?></label>
                            <select name="r_submission_login_required" class="form-control">
                             <option value="1">No</option>
                             <option value="2"<?php echo $recipe_opts['recipe_submission_login_required'] == 2 ? "SELECTED": ""; ?>>Yes</option>  
                           </select>  
                         </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"><?php _e( 'Update', 'recipe' ); ?></button>
                    </div>
                    </form>
                  </div>
                  
              </div>
              <hr>
<!--              <form action="options.php" method="post">-->
                  <?php
                  
//                  settings_fields( 'r_opts_groups' );
//                    
//                  do_settings_sections( 'r_opts_sections' ); //creates section in current page
//            
//                  submit_button();
//                  
                  ?>
                  
<!--              </form>-->
               
           </div>
            
             <?php         
        }