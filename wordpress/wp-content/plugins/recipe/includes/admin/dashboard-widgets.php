<?php


        function r_add_dashboard_widgets(){
            
            wp_add_dashboard_widget(
            
            'r_latest_recipe_rating_widget',//id
            'latest_recipe_ratings',       //name
            'r_latest_recipe_rating_dislay'//callback function
                
            );
            
        }



        function r_latest_recipe_rating_dislay(){
            
            global $wpdb;
            
            $table                                      =   $wpdb->prefix . "recipe_ratings";
            
            $sql                                        =   "SELECT * FROM $table ORDER BY ID DESC LIMIT 5";
            
            $latest_ratings                             =   $wpdb->get_results( $sql );
            
            echo '<ul>';
            
            foreach( $latest_ratings as $rating ){
                
               $title                                   =   get_the_title( $rating->recipe_id );
               $permalink                               =   get_the_permalink( $rating->recipe_id );
               $rating                                  =   $rating->rating;
              ?>  
               <li>    
               <a href="<?php echo $permalink; ?>">
                  
                   Your recipe <?php echo $title;  ?> </a>  
                   has received a rating of <?php echo $rating;  ?>
                            
               </li>
              
               <?php 
            }
            
            echo '</ul>';
            
            
        }