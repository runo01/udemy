<?php


        function r_origin_add_form_fields(){
            
            ?>
            <div class="form-field">
                <label><?php _e( 'More info URL', 'recipe' ) ?></label>
                <input type="text" name="r_more_info_url">
                <p class="description">
                  <?php
                    esc_html_e(
                    'A URL a user can click to get more information about this origin',
                    'recipe'
                    
                    );  
                    ?>  
                </p>
            </div>
            <?php
            
        }


        
        function r_origin_edit_form_fields( $term ){
            $url                =   get_term_meta( $term->term_id, 'taxonomy', true );
            
            ?>
         <tr>
             <th scope="row" valign="top">
               <label><?php _e( 'More info URL', 'recipe' ) ?></label>  
             </th>
             <td>
               <input type="text" name="r_more_info_url" value="<?php echo esc_attr( $url ); ?>"> 
                <p class="description">
                  <?php
                    esc_html_e(
                    'A URL a user can click to get more information about this origin',
                    'recipe'
                    
                    ); 
            
                    ?>  
                </p> 
             </td>
         </tr>
            <?php
            var_dump($term);
        } 