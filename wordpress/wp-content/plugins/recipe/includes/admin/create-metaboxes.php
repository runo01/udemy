<?php 


function r_create_metaboxes() {
       
    add_meta_box(
        
        'r_recipe_options_mb',// id
        __( 'Recipe Options', 'recipe' ),// translation
        'r_recipe_options_mb',// callback function of what goes on in the created meta box
        'recipe',//post type
        'normal',//position in admin normal is under
        'high'//priority
        
        );
    }