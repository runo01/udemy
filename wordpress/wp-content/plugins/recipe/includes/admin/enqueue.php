<?php 

    function r_admin_enqueue() {
       
        global $typenow;
        //this would ensure that the post-type is recipe and r_plugin_opts page is loaded so the enqueued style doesnt mess with other post-types
        if ( $typenow != 'recipe' && ( !isset($_GET['page']) || $_GET['page'] != 'r_plugin_opts' ) ){
            
            return;
        }
        
        wp_register_style( 
            //pluginsurl accceps 2 parameters location of stylesheet and locarion of plugin
            'ru_bootstrap', 
            plugins_url( '/assets/styles/bootstrap.css', RECIPE_PLUGIN_URL )
            
            );
        
        wp_enqueue_style( 'ru_bootstrap' );
        
        
         wp_register_script( 
            //pluginsurl accceps 2 parameters location of stylesheet and locarion of plugin
            'r_admin_optins', 
            plugins_url( '/assets/scripts/options.js', RECIPE_PLUGIN_URL ), array('jquery'), '1.0.0', true
            
            );
        wp_enqueue_script( 'r_admin_optins' );
        
        
     }