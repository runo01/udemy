jQuery(function($) {

	//RECIPE RATING
	$( "#recipe_rating" ).bind( "rated", function() {
		
		$(this).rateit( "readonly", true );
		
		var form 						=	{
				
			action:							"r_rate_recipe",
			rid:							$(this).data( "rid" ),
			rating:							$(this).rateit( "value" )
		}
		//recipe_obj is the name of the localized ajax script registed url for running ajax commands from this file registed in front enqueue
		$.post( recipe_obj.ajax_url, form, function(data){
			
			
			
		});
	});
    
    var featured_frame                   =  wp.media({
        
        title:                              'Select or Upload Media',
        button:                             {
            text:                           'Use this Media'   
        },
        multiple:                           false   
    });
    
    $('#recipe-img-upload-btn').on('click', function(e){
        
        e.preventDefault();
        featured_frame.open();
    });
    
    featured_frame.on('select', function(){
        
        var attachment                  =   featured_frame.state().get( 'selection' ).first().toJSON();
        
        $('#recipe-img-preview').attr('src', attachment.url);
        $('#r_inputImgID').val( attachment.id);
    });
    //RECIPE CREATION
        $('#recipe-form').on('submit', function(e){
            
            e.preventDefault();
            
            $(this).hide();
            
            $('#recipe-status').html('<div class="alert alert-info text-center">Pls Wait!</div>');
            
            
            
            var form                       =    {
                //create a unique id used to add ajax 'r_submit_user_recipe'action hook
                action:                         "r_submit_user_recipe",
                content:                        tinymce.activeEditor.getContent(),
                video_url:                      $('#r_inputVideoUrl').val(),
                title:                          $('#r_inputTitle').val(),
                ingredients:                    $('#r_inputIngredients').val(),
                time:                           $('#r_inputTime').val(),
                utensils:                       $('#r_inputUtensils').val(),
                level:                          $('#r_inputLevel').val(),
                meal_type:                      $('#r_inputMealType').val(),
                attachment_id:                  $('#r_inputImgID').val()
                
            }
            //recipe_obj is the name of the localized ajax script registed url for running ajax commands from this file registed in front enqueue
            $.post( recipe_obj.ajax_url, form).always(function(response){
                
                if ( response.status        ==  2 ){
                    
                    $('#recipe-status').html('<div class="alert alert-info text-center">Recipe Submitted Successfully!</div>');
                    
                } else {
                    
                    $('#recipe-status').html('<div class="alert alert-danger text-center">Unable to Submit Recipe, Pls Fill in All Fields </div>');
                    
                    $('#recipe-form').show();
                }
                
            });
            
        });
    //REGISTRATION AUTH
         $('#register-form').on('submit', function(e){
            
            e.preventDefault();
             
             $(this).hide();
            
            $('#register-status').html('<div class="alert alert-info text-center">Pls Wait!</div>');
             
             
            
            var form                       =    {
                //create a unique id used to add ajax 'r_submit_user_recipe'action hook
                action:                         "r_submit_user_registration",
                name:                           $('#register-form-name').val(),
                email:                          $('#register-form-email').val(),
                username:                       $('#register-form-username').val(),
                pass:                           $('#register-form-password').val(),
                confirm_pass:                   $('#register-form-repassword').val(),
                _wpnonce:                       $('#_wpnonce').val()
                
                
                                
            }
            //recipe_obj is the name of the localized ajax script registed url for running ajax commands from this file registed in front enqueue
            $.post( recipe_obj.ajax_url, form).always(function(response){
                
                if ( response.status        ==  2 ){
                    
                    $('#register-status').html('<div class="alert alert-info text-center"> Successfully!</div>');
                    
                    location.href           =   recipe_obj.home_url;
                    
                } else {
                    
                    $('#register-status').html('<div class="alert alert-danger text-center">Unable to Submit Registration, Pls try with a different email/password </div>');
                    
                    $('#register-form').show();
                }
                
            });
            
        });
    //LOGIN AUTH
       $('#login-form').on('submit', function(e){
            
            e.preventDefault();
             
             $(this).hide();
            
            $('#login-status').html('<div class="alert alert-info text-center">Pls Wait while we log you in!</div>');
             
             
            
            var form                       =    {
                //create a unique id used to add ajax 'r_submit_user_recipe'action hook
                
                action:                         "recipe_user_login",
                username:                       $('#login-form-username').val(),
                pass:                           $('#login-form-password').val(),
                _wpnonce:                       $('#_wpnonce').val()
                                 
            }
            //recipe_obj is the name of the localized ajax script registed url for running ajax commands from this file registed in front enqueue
            $.post( recipe_obj.ajax_url, form).always(function(data){
                
                if ( data.status        ==  2 ){
                    
                    $('#login-status').html('<div class="alert alert-info text-center"> Successfully!</div>');
                    
                    location.href           =   recipe_obj.home_url;
                    
                } else {
                    
                    $('#login-status').html('<div class="alert alert-danger text-center">Unable log you in, username or password does not match please try again </div>');
                    
                    $('#login-form').show();
                }
                
            });
            
        });
    
    
});