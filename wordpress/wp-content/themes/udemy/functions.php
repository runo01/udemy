<?php

//Setup




//Includes
include ( get_template_directory() . '/includes/front/enqueue.php' );
include ( get_template_directory() . '/includes/setup.php' );
include ( get_template_directory() . '/includes/widgets.php' );
include ( get_template_directory() . '/includes/theme-customizer.php' );
include ( get_template_directory() . '/includes/customizer/social.php' );
include ( get_template_directory() . '/includes/customizer/misc.php' );
include ( get_template_directory() . '/includes/customizer/enqueue.php' );
include ( get_template_directory() . '/includes/buddypress/xprofile-cover-image.php' );
include ( get_template_directory() . '/includes/buddypress/bp-profile-tabs.php' );
include ( get_template_directory() . '/includes/functions.php' );
include ( get_template_directory() . '/includes/buddypress/profile-posts.php' );
include ( get_template_directory() . '/includes/woocommerce/checkout-fields.php' );
include ( get_template_directory() . '/includes/woocommerce/shipping-fields.php' );
include ( get_template_directory() . '/includes/mobile.php' );
include ( get_template_directory() . '/includes/admin/author-fields.php' );
include ( get_template_directory() . '/includes/avatar.php' );
include ( get_template_directory() . '/includes/home-query.php' );
require_once ( get_template_directory() . '/includes/libs/class-tgm-plugin-activation.php' );
include ( get_template_directory() . '/includes/register-plugins.php' );



//Hooks
add_action( 'wp_enqueue_scripts', 'ru_enqueue' );
add_action( 'after_setup_theme', 'ru_setup_theme' );
add_action( 'widgets_init', 'ru_widgets');
add_action( 'customize_register', 'ru_customize_register' );
add_action( 'customize_preview_init', 'ru_customize_preview_init' );
add_filter( 'bp_before_xprofile_cover_image_settings_parse_args', 'ru_xprofile_cover_image' );
add_filter( 'bp_before_groups_cover_image_settings_parse_args', 'ru_xprofile_cover_image' );
add_action( 'bp_setup_nav', 'ru_bp_profile_tabs' );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
add_filter( 'woocommerce_billing_fields', 'ru_wc_billing_fields' );
add_filter( 'woocommerce_shipping_fields', 'ru_wc_shipping_fields' );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
add_filter( 'excerpt_more', '__return_false' );
add_action( 'excerpt_length', 'ru_excerpt_length' );
add_action( 'show_user_profile', 'ru_custom_user_fields' );
add_action( 'edit_user_profile', 'ru_custom_user_fields' );
add_action( 'personal_options_update', 'ru_save_extra_options_fields' );
add_action( 'edit_user_profile_update', 'ru_save_extra_options_fields' );
add_filter( 'avatar_defaults', 'ru_new_avartar' );
add_action( 'pre_get_posts', 'ru_modify_homepage_query' );
add_action( 'tgmpa_register', 'ru_register_required_plugins' );





//Shortcodes