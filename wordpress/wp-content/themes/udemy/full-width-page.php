
<?php  

	/*
	 * Template Name: Full Width Page
	 * 
	 *
	 *
	 */
get_header(); 


?>
    
	<!-- Header
    <!-- #header end -->
	
	<section id="page-title">

        <div class="container clearfix">
            <h1 class = "text-center"><?php echo $pagename; ?></h1>
            
            <?php if ( function_exists('the_subtitle') ) : ?>
            
            <span class = "text-center"><?php the_subtitle(); ?></span>
            
            <?php endif; ?>
        </div>

    </section><!-- #page-title end -->
    

	<!-- Content
    ============================================= -->
	<section id="content">

		<div class="content-wrap">

			<div class="section header-stick bottommargin-lg clearfix" style="padding: 20px 0;">
				<div>
					
				</div>
			</div>

			<div class="container clearfix">

				<!-- Post Content
                ============================================= -->
				

				
				
				<?php while ( have_posts()) : the_post(); 
				    
				    $author_id                          =   get_the_author_meta('ID');
				    $author_url                         =   get_author_posts_url('$author_id');
				    
				    
				    
				    
				    
				?>
				
				
				
				<div class="single-post nobottommargin">

                        <!-- Single Post
                        ============================================= -->
                        <div class="entry clearfix">

                          

                            <!-- Entry Meta
                            ============================================= -->
                            
                            <!-- Entry Image
                            ============================================= -->
                            <div class="entry-image">
                                <?php  if ( has_post_thumbnail() ):?>
							
								<a href="<?php the_permalink(); ?>" >
								<img src="" alt="" />
								<?php  the_post_thumbnail('full'); ?>
								
								</a>
								<?php endif; ?>
                            </div><!-- .entry-image end -->

                            <!-- Entry Content
                            ============================================= -->
                            <div class="entry-content notopmargin">

                                <?php
                                
                                the_content(); 
                                wp_link_pages(array(
                                    'before'           => '<p class= "text-center">' . __( 'Pages:' ),
                                    'after'            => '</p>',
                                   
                                ));
                                
                                ?>
                                <!-- Post Single - Content End -->

                                <!-- Tag Cloud
                                ============================================= -->
                                <div class="tagcloud clearfix bottommargin">
                                   <?php the_tags( ''. ' '); ?>
                                </div><!-- .tagcloud end -->

                                <div class="clear"></div>

                            </div>
                        </div><!-- .entry end -->

                        <!-- Post Navigation
                        ============================================= -->
                        <div class="post-navigation clearfix">

                            <div class="col_half nobottommargin">
                                <?php previous_post_link(); ?>
                            </div>

                            <div class="col_half col_last tright nobottommargin">
                                <?php next_post_link(); ?>
                            </div>

                        </div><!-- .post-navigation end -->

                        <div class="line"></div>

                        <!-- Post Author Info
                        ============================================= -->
                       <!-- Post Single - Author End -->

                        <div class="line"></div>
                       

                        </div>
<!--                         first if have_posts end primary loop -->
							<?php endwhile;  ?>
							
							
							<?php 
							
							if ( comments_open() || get_comments_number() ){
							    
							      comments_template();
							}
							
							
							
							?>
							
							
						<!-- #comments end -->

                 
					<!-- .pager end -->

				</div><!-- .postcontent end -->

				<!-- Sidebar
                ============================================= -->
			
			</div>

		
	</section><!-- #content end -->

	<?php  get_footer(); ?>