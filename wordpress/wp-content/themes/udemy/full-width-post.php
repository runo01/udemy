
<?php  
   /*
    * Template Name: Full Width Post
    * 
    * Template Post Type: Post
    * 
    * 
    * 
    */


get_header(); 

?>
	<!-- Header
    
	<!-- Content
    ============================================= -->
	<section id="content">

		<div class="content-wrap">

			<div class="section header-stick bottommargin-lg clearfix" style="padding: 20px 0;">
				<div>
					<div class="container clearfix">
						<span class="label label-danger bnews-title">Breaking News:</span>

						<div class="fslider bnews-slider nobottommargin" data-speed="800" data-pause="6000" data-arrows="false" data-pagi="false">
							<div class="flexslider">
								<div class="slider-wrap">
									<div class="slide"><a href="#"><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </strong></a></div>
									<div class="slide"><a href="#"><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </strong></a></div>
									<div class="slide"><a href="#"><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </strong></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container clearfix">

				<!-- Post Content
                ============================================= -->
				

				<?php  if ( have_posts() ) : ?>
				
				<?php while ( have_posts()) : the_post(); 
				    
				    $author_id                          =   get_the_author_meta('ID');
				    $author_url                         =   get_author_posts_url('$author_id');
				    
				    
				    
				    
				    
				?>
				
				
				
				<div class="single-post nobottommargin">

                        <!-- Single Post
                        ============================================= -->
                        <div class="entry clearfix">

                            <!-- Entry Title
                            ============================================= -->
                            <div class="entry-title">
                                <h2><?php the_title(); ?></h2>
                            </div><!-- .entry-title end -->

                            <!-- Entry Meta
                            ============================================= -->
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> <?php get_the_date(); ?></li>
                                <li><a href="<?php echo $author_url; ?>"><i class="icon-user"></i> <?php the_author(); ?></a></li>
                                <li><i class="icon-folder-open"></i> <a href="#"> <?php the_category( ' ' ); ?></a>, <a href="#">Media</a></li>
                                <li><a href="#"><i class="icon-comments"></i> <?php comments_number( '0' ); ?> Comments</a></li>
                            </ul><!-- .entry-meta end -->

                            <!-- Entry Image
                            ============================================= -->
                            <div class="entry-image">
                                <?php  if ( has_post_thumbnail() ):?>
							
								<a href="<?php the_permalink(); ?>" >
								<img src="" alt="" />
								<?php  the_post_thumbnail('full'); ?>
								
								</a>
								<?php endif; ?>
                            </div><!-- .entry-image end -->

                            <!-- Entry Content
                            ============================================= -->
                            <div class="entry-content notopmargin">

                                <?php
                                
                                the_content(); 
                                wp_link_pages(array(
                                    'before'           => '<p class= "text-center">' . __( 'Pages:' ),
                                    'after'            => '</p>',
                                   
                                ));
                                
                                ?>
                                <!-- Post Single - Content End -->

                                <!-- Tag Cloud
                                ============================================= -->
                                <div class="tagcloud clearfix bottommargin">
                                   <?php the_tags( ''. ' '); ?>
                                </div><!-- .tagcloud end -->

                                <div class="clear"></div>

                            </div>
                        </div><!-- .entry end -->

                        <!-- Post Navigation
                        ============================================= -->
                        <div class="post-navigation clearfix">

                            <div class="col_half nobottommargin">
                                <?php previous_post_link(); ?>
                            </div>

                            <div class="col_half col_last tright nobottommargin">
                                <?php next_post_link(); ?>
                            </div>

                        </div><!-- .post-navigation end -->

                        <div class="line"></div>

                        <!-- Post Author Info
                        ============================================= -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Posted by <span><a href="<?php echo $author_url; ?>"><?php the_author(); ?></a></span></h3>
                            </div>
                            <div class="panel-body">
                                <div class="author-image">
                                    <?php echo  get_avatar( $author_id, '90', '', false, array( 'class'   => 'image-circle')); ?>
                                </div>
                               <?php echo nl2br(get_the_author_meta('description')); ?>
                               </div>
                        </div><!-- Post Single - Author End -->

                        <div class="line"></div>

                        <h4><?php //the_author_posts_link(); ?></h4>

                        <div class="related-posts clearfix">
                        
                        <?php 
                        
                        $categories                         =   get_the_category();
                        
                        $rp_query                           =   new Wp_Query(array(
                            
                            'posts_per_page'                =>  2,
                            'post__not_in'                  =>  array( $post->ID ),
                            'cat'                           => $categories[0]->term_id
                             
                        ));
                        
                        if ( $rp_query->have_posts() ):
                        while ( $rp_query->have_posts() ):
                                $rp_query->the_post();
                        
                        ?>
                        

                            <div class="mpost clearfix">
                                <div class="entry-image">
                                <?php if ( has_post_thumbnail() ) : ?>
                                
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail' ) ?></a>
                                    
                                <?php endif; ?>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="#"><?php the_title(); ?></a></h4>
                                    </div>
                                    <ul class="entry-meta clearfix">
                                        <li><i class="icon-calendar3"></i> <?php get_the_date(); ?></li>
                                        <li><a href="#"><i class="icon-comments"></i> <?php comments_number( '0' ); ?></a></li>
                                    </ul>
                                    <div class="entry-content"><?php the_excerpt(); ?></div>
                                </div>
                            </div>

						<?php
						
// 			              end secondary loop -->
						endwhile; 
						
						wp_reset_postdata();
						
						 endif; 
						  
						 ?>
                            

                        </div>
<!--                         first if have_posts end primary loop -->
							<?php endwhile; else: ?>
							
							<?php endif; ?>
							<?php 
							
							if ( comments_open() || get_comments_number() ){
							    
							      comments_template();
							}
							
							
							
							?>
							
							
						<!-- #comments end -->

                    </div>
					<!-- .pager end -->

				<!-- .postcontent end -->

				<!-- Sidebar
                ============================================= -->
				
			</div>

		</div>

	</section><!-- #content end -->

	<?php  get_footer(); ?>