<?php

function ru_setup_theme () {
      
   add_theme_support( 'post-thumbnails' );
   
   add_theme_support( 'title-tag' );
   
   add_theme_support( 'custom-logo' );
   
   add_theme_support( 'automatic-feed-links' );
   
   add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
   
   add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) ); 
   
   add_theme_support( 'woocommerce' );
   
   // Image Sizes
   add_image_size( 'ru-post-image', 860, 575, false );
   
   
   // define and register starter content to be used to showcase theme in a new site
   $starter_content                         =   array(
       
        'widgets'                           =>  array(
           'ru_sidebar'                     =>  array(
               
               'text_business_info',
               'search',
               'text_about',
           ) 
            
        ),
      
       //this creates the custom image attachments to be used as post thumbnails for posts
       'attachments'                        =>  array(
           'image_about'                    =>  array(
               
               'post_title'                 =>  __( 'About', 'udemy' ),
               'file'                       =>  'assets/images/about/1.jpg',
           )
           
       ),
       
       // this specifies the core-defined pages and add thumbnails to some of them
       'posts'                              =>  array(
           
           'home'                           =>  array(
               
                'thumbnail'                 =>  '{{ image-about }}',
           ),
           
           'about'                         =>  array(
               
               'thumbnail'                 =>  '{{ image-about }}',
           ),
           
           'contact'                       =>  array(
               
               'thumbnail'                 =>  '{{ image-about }}',
           ),
           
           'blog'                          =>  array(
               
               'thumbnail'                 =>  '{{ image-about }}',
           ),
           
           'homepage-section'              =>  array(
               
               'thumbnail'                 =>  '{{ image-about }}',
           ),
           
           
           
       ),
       
       // this creates a default static front page and assigns the front page and post pages
       'options'                           =>   array(
           
           'show_on_front'                 =>   'page',
           'page_on_front'                 =>   '{{home}}',
           'page_for_posts'                =>   '{{blog}}',
       ),
       
       // sets the front page custom theme mods to the IDs of the core registered pages
       'theme_mods'                        =>   array(
           
           'ru_facebook_handle'            =>   'udemy',
           'ru_twitter_handle'             =>   'udemy',
           'ru_instagram_handle'           =>   'udemy',
           'ru_telephone_handle'           =>   'udemy',
           'ru_email_handle'               =>   'udemy',
           'ru_header_show_search'         =>   'udemy',
           'ru_header_show_cart'           =>   'udemy',
       ),
       
       //sets the nav menus to each of the register places on the theme
       'nav_menus'                         =>   array(
           
           'primary'                       =>   array(
               'name'                      =>   __( 'Primary Menu', 'udemy' ),
               'items'                     =>   array(
                   
                   'link_home',
                   'page_about',
                   'page_blog',
                   'page_contact',
               ),
               
               
           ),
           
           'secondary'                       =>  array(
               'name'                       =>  __( 'Secondary Menu', 'udemy' ),
               'items'                      =>  array(
                   
                   'link_home',
                   'page_about',
                   'page_blog',
                   'page_contact',
               ),
           ),            
           
       ),
       
            
   );
   
   
   add_theme_support( 'starter-content', $starter_content);
    
    register_nav_menu (
        
        'primary', __( 'Primary Menu', 'udemy' )
        
        );
    register_nav_menu (
        
        'secondary', __( 'Secondary Menu', 'udemy' )
        
        );
    
     if ( function_exists( 'quads_register_ad') ){
        quads_register_ad( array('location' => 'udemy_header', 'description' => 'Udemy Header position') );
       
    }
    
    if ( ! isset( $content_width ) ) $content_width = 1200;
}
