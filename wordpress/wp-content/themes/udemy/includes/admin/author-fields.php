<?php 


        function ru_custom_user_fields( $user ) {
      ?>      
    <table class="table-form">
    	<tr>
    		<th>
    			<label for="ru_twitter"><?php  _e( 'Twitter', 'udemy' ); ?></label>
    		</th>
    		<td>
    			<input type="text" class="regular-text" name="ru_twitter" id="ru_twitter" 
    				   value="<?php echo esc_attr(get_the_author_meta( 'ru_twitter', $user->ID) ); ?>" />
    		</td>
    	</tr>
    </table>
    
    <?php 
   
    
        }
        
        function ru_save_extra_options_fields($user_id) {
            
//             $user = wp_get_current_user();
//             echo '<pre>';
//             print_r($user);
//             echo '</pre>';
           
          
            if (!current_user_can( 'edit_user' )) {
                
                return;
            }
            
            update_user_meta( $user_id, 'ru_twitter', $_POST['ru_twitter'], '' );
      }