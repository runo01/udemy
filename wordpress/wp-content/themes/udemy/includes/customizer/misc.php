<?php 

    function ru_misc_customizer_section( $wp_customize ) {
        //settings
        $wp_customize->add_setting( 'ru_header_show_search', array( 
           
                    'default'                         =>  'yes',
                    'transport'                       =>  'postMessage',
           
       ));
       
        $wp_customize->add_setting( 'ru_header_show_cart', array(
           
                    'default'                         =>  'yes',
                    'transport'                       =>  'postMessage',
       ));
       
        $wp_customize->add_setting( 'ru_footer_copywrite_text', array(
           
                    'default'                         =>  'Copyrights &copy; 2017 All Rights Reserved by Omene Joseph Inc.',
                    
       ));
       
        $wp_customize->add_setting( 'ru_footer_tos_page', array(
           
                    'default'                         =>  0,
           
       ));
       
        $wp_customize->add_setting( 'ru_footer_piracy_page', array(
           
                    'default'                         =>  0,
           
       ));
        
        $wp_customize->add_setting( 'ru_show_header_popular_posts_widget', array(
            
            'default'                         =>  false,
           
        ));
        
        $wp_customize->add_setting( 'ru_show_header_popular_posts_title', array(
            
            'default'                         =>  'Breaking News',
            
        ));
       
       // section
        $wp_customize->add_section( 'ru_misc_section', array(
           
                    'title'                         =>  __( 'Udemy Misc Settings', 'udemy' ),
                    'priority'                      =>  30,
                    'panel'                         =>  'udemy'
           
       ));
       
       $wp_customize->add_control(
           
           new WP_Customize_Control( $wp_customize, 'ru_header_show_search_input', array(
               
               'label'          =>  __( 'Show Search Button in Header', 'udemy' ),
               'section'        =>  'ru_misc_section',
               'settings'       =>  'ru_header_show_search',
               'type'           =>  'checkbox',
               'choices'        =>  array(
                   
                   'yes'        =>  __('Yes', 'udemy'),
                   
               )
               
               
           )
               )
           
           
           );
       
       $wp_customize->add_control(
           
           new WP_Customize_Control( $wp_customize, 'ru_header_show_cart_input', array(
               
               'label'          =>  __( 'Show cart in Header', 'udemy' ),
               'section'        =>  'ru_misc_section',
               'settings'       =>  'ru_header_show_cart',
               'type'           =>  'checkbox',
               'choices'        =>  array(
                   
                   'yes'        =>  __('Yes', 'udemy'),
                   
               )
               
               
           )
               )
           
           
           );
       
       $wp_customize->add_control(
           
           new WP_Customize_Control( $wp_customize, 'ru_footer_copywrite_text_input', array(
               
               'label'          =>  __( 'Copywrite Text', 'udemy' ),
               'section'        =>  'ru_misc_section',
               'settings'       =>  'ru_footer_copywrite_text',
               'type'           =>  'textarea'
                                                 
               )
               
               
           )
               );
       
       $wp_customize->add_control(
           
           new WP_Customize_Control( $wp_customize, 'ru_footer_tos_page_input', array(
               
               'label'          =>  __( 'Terms Of Service Pages', 'udemy' ),
               'section'        =>  'ru_misc_section',
               'settings'       =>  'ru_footer_tos_page',
               'type'           =>  'dropdown-pages',
               
           )
               )
           
           
           );
       
       $wp_customize->add_control(
           
           new WP_Customize_Control( $wp_customize, 'ru_footer_piracy_page_input', array(
               
               'label'          =>  __( 'Privacy Policy Page', 'udemy' ),
               'section'        =>  'ru_misc_section',
               'settings'       =>  'ru_footer_piracy_page',
               'type'           =>  'dropdown-pages',
              
               
           )
               )
           
           
           );
       
       $wp_customize->add_control(
           
           new WP_Customize_Control( $wp_customize, 'ru_show_header_popular_posts_widget_input', array(
               
               'label'          =>  __( 'Show Popular Posts Widget in Header', 'udemy' ),
               'section'        =>  'ru_misc_section',
               'settings'       =>  'ru_show_header_popular_posts_widget',
               'type'           =>  'checkbox',
               'choices'        =>  array(
                   
                   'yes'        =>  __('Yes', 'udemy'),
                   
               )
               
               
           )
               )
           
           
           );
       
       $wp_customize->add_control(
           
           new WP_Customize_Control( $wp_customize, 'ru_show_header_popular_posts_title_input', array(
               
               'label'          =>  __( 'Popular Post Title', 'udemy' ),
               'section'        =>  'ru_misc_section',
               'settings'       =>  'ru_show_header_popular_posts_title',
                              
           )
               
               
               )
           );
          
    }