<?php 

function ru_bp_profile_tabs() {
       
    if ( !ru_plugin_activated_check( 'recipe/index.php' ) ) {
        return;
    }
    
    global $bp;
        
        bp_core_new_nav_item([
            
            'name'                      =>  __( 'Recipes', 'recipe' ),
            'slug'                      =>  'recipe',
            'postion'                   =>  100,
            'screen_function'           =>  'ru_recent_recpes_tab',
            'show_for_displayed_user'   =>  true,
            'item_css_id'               =>  'ru_user_recipe',
            'default_subnav_slug'       =>  'recipes'
        ]);
        
    }