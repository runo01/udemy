
<?php  get_header(); ?>
	<!-- Header
    ============================================= -->

					<!-- Top Cart
						============================================= -->
			<!-- #header end -->

	<!-- Content
    ============================================= -->
	<section id="content">

		<div class="content-wrap">

			<div class="section header-stick bottommargin-lg clearfix" style="padding: 20px 0;">
				

			<div class="container clearfix">

				<!-- Post Content
                ============================================= -->
				<div class="postcontent nobottommargin clearfix">

				<?php  if ( have_posts() ) : ?>
				
				<?php while ( have_posts()) : the_post(); 
				    
				    $author_id                          =   get_the_author_meta('ID');
				    $author_url                         =   get_author_posts_url('$author_id');
				    
				    
				    
				    
				    
				?>
				
				
				
				<div id=" post-<?php the_ID(); ?> " <?php  post_class( 'single-post nobottommargin' ); ?>>

                        <!-- Single Post
                        ============================================= -->
                        <div class="entry clearfix">

                            <!-- Entry Title
                            ============================================= -->
                            <div class="entry-title">
                                <h2><?php the_title(); ?></h2>
                            </div><!-- .entry-title end -->

                            <!-- Entry Meta
                            ============================================= -->
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> <?php get_the_date(); ?></li>
                                <li><a href="<?php echo $author_url; ?>"><i class="icon-user"></i> <?php the_author(); ?></a></li>
                                <li><i class="icon-folder-open"></i> <a href="#"> <?php the_category( ' ' ); ?></a>, <a href="#">Media</a></li>
                                <li><a href="#"><i class="icon-comments"></i> <?php comments_number( '0' ); ?> Comments</a></li>
                            </ul><!-- .entry-meta end -->

                            <!-- Entry Image
                            ============================================= -->
                            <div class="entry-image">
                                <?php  if ( has_post_thumbnail() ):?>
							
								<a href="<?php the_permalink(); ?>" >
								<img src="" alt="" />
								<?php  the_post_thumbnail('full'); ?>
								
								</a>
								<?php endif; ?>
                            </div><!-- .entry-image end -->

                            <!-- Entry Content
                            ============================================= -->
                            <div class="entry-content notopmargin">
                                
                                <p>
                                
                                Release Date: <?php the_field( 'release_date' ); ?><br>
                                Purchase URL:
                                <a href="<?php the_field( 'purchase_url' ); ?>">
                                <?php the_field( 'purchase_url' ); ?>
                                </a>
                                
                            </p>


                                <?php
                                
                                the_content(); 
                                wp_link_pages(array(
                                    'before'           => '<p class= "text-center">' . __( 'Pages:' ),
                                    'after'            => '</p>',
                                   
                                ));
                                
                                ?>
                                <!-- Post Single - Content End -->

                                <!-- Tag Cloud
                                ============================================= -->
                                <div class="tagcloud clearfix bottommargin">
                                   <?php the_tags( ''. ' '); ?>
                                </div><!-- .tagcloud end -->

                                <div class="clear"></div>

                            </div>
                        </div><!-- .entry end -->

                        <!-- Post Navigation
                        ============================================= -->
                        <!-- .post-navigation end -->

                        <div class="line"></div>

                        <!-- Post Author Info
                        ============================================= -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Posted by <span><a href="<?php echo $author_url; ?>"><?php the_author(); ?></a></span></h3>
                            </div>
                            <div class="panel-body">
                                <div class="author-image">
                                    <?php echo  get_avatar( $author_id, '90', '', false, array( 'class'   => 'image-circle')); ?>
                                </div>
                               <?php echo nl2br(get_the_author_meta('description')); ?>
                               </div>
                        </div><!-- Post Single - Author End -->

                        <div class="line"></div>

                        <h4><?php //the_author_posts_link(); ?></h4>

                        
<!--                         first if have_posts end primary loop -->
							<?php endwhile; else: ?>
							
							<?php endif; ?>
							
							
							
						<!-- #comments end -->

                    </div>
					<!-- .pager end -->

				</div><!-- .postcontent end -->

				<!-- Sidebar
                ============================================= -->
				<?php get_sidebar() ;?>
			</div>

		</div>

	</section><!-- #content end -->

	<?php  get_footer(); ?>