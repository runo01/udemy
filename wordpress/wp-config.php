<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'udemy');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '60kC7T!N!WaA5/E78S@76NtI4sNmld[P[xW#70JC?HHHSAMuh&dX}+e1p2$k*EQ`');
define('SECURE_AUTH_KEY',  'J0=)iZ*]%^!KX:^ qHv|w4pF!+^,HqH1:/tO5f(n7.R;2Y@Lmq2o>GAu(Jg&ibc[');
define('LOGGED_IN_KEY',    'zZ71YJw(BB Nx6N3k5<rO-7<pj-ho(|*.}A<l-<`nLee]R#uJwTs7ju85#XWxHHj');
define('NONCE_KEY',        'ws0qQFbD}t[r#K}RV%{I23;i.d0y($4n$@]60_L|7x@NpK7>H $Ohn2Mtx2AaU3L');
define('AUTH_SALT',        'qME]QS8E:V$sX)6A4C`8:>Ym6+KdY5&*dYISmYZuYpPDO}>8Fen-v4P=<@IilR$8');
define('SECURE_AUTH_SALT', '1Cr@M?3.pat6AN<CFBYl:&oYn/@zIVh{*bX2g _K4Y7)7>e1=`|NS*U]BXe6K+0.');
define('LOGGED_IN_SALT',   'eu%^[Ln7 O5YDc&*dtG+j!7d.4!1WAxL:9QW|1eGZOd|(Ix=|m.Y`v2,iW/9ddZ[');
define('NONCE_SALT',       'J Ie|_^)Z`:5Y6swxKtSXz.@#wQ*0kAgFKiiVZ7Q,gn0H )7)TS-v0DWyP{.MKu8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true );

define('SAVEQUERIES', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

